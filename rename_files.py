import argparse
import os


parser = argparse.ArgumentParser()
parser.add_argument("-d", "--dry-run", default=False, action="store_true")

args = parser.parse_args()


target = "overlap"
new_part = "overlap-filtered"

extensions = {".yml", ".yaml", ".pkl", ".h5", ".hdf5", ".pth"}
filenames = os.listdir()


list_of_files = list(os.listdir())

for filename in list_of_files:
    if not new_part in filename:

        _, ext = os.path.splitext(filename)

        if ext in extensions:

            if target in filename:
                new_filename = filename.replace(target, new_part)

                print(f"{filename} -> {new_filename}")

                if not args.dry_run:
                    os.rename(filename, new_filename)
